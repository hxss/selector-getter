
((factory) => {
	if (typeof define === 'function' && define.amd) {
		define(['@hxss/recursive-proxy'], factory);
	} else if (typeof exports !== 'undefined') {
		module.exports = factory(require('@hxss/recursive-proxy'));
	} else {
		factory(RecursiveProxy);
	}
})((RecursiveProxy) => {

	const SelectorGetter = (() => {
//		const test = 'body .listicle__content.sdfsd, div#article54.article>.content ~ [href = "href_val"] + input:checked + .label:before, [data-attr=data_val].jkh,mn p.h1:nth-child(n+1), :not(.class, #body>selector)';
//		const test = 'div#article54.article[data-name="data_val"]:not(n+1)';

		const excludeAttrPsd = '(([\\(\\[].*?[\\]\\)])(\\5)?|(\\2)?';
		const regex = {
			elements: new RegExp(excludeAttrPsd + '([, >+~]+))', 'g'),
			tags: /^(.{0})(.*[\])])?(\w+|\*)/g,
			ids: new RegExp(excludeAttrPsd + '#([\\w-]+))', 'g'),
			classes: new RegExp(excludeAttrPsd + '\\.([\\w-]+))', 'g'),
			attrs: /\[([\w-]+)\s*[=!^$*~|]*\s*(["']?([\w-]+)["']?)?\]/g,
			datas: /\[data-([\w-]+)\s*[=!^$*~|]*\s*(["']?([\w-]+)["']?)?\]/g,
			pseudos: /:([\w-]+)(\((.*)\))?/g,
		};

		class SelectorGetter {
			static regex() {
				return regex;
			}
			get regex() {
				return regex;
			}

			constructor(selector) {
				this.selector = selector;

//				this._elements = undefined;
//				this._tags     = undefined;
//				this._ids      = undefined;
//				this._classes  = undefined;
//				this._attrs    = undefined;
//				this._datas    = undefined;
//				this._pseudos  = undefined;

				this.complexProps = ['attrs', 'datas', 'pseudos'];

				this.proxy = new RecursiveProxy(this, {
					getObject: function() {
						return this.curTarget.length == 1
							? this.proxy[0]
							: this.proxy;
					},
					isObject: val => typeof val === 'object',
				})
			}

			get s() {
				return this.selector;
			}
			toString() {
				return this.s;
			}
			valueOf() {
				return this.s;
			}

			get elements() {
				if (!this._elements) {
					this._elements = this.s.replace(
						this.regex.elements,
						(...a) => (a[2] || '') + (a[4] || '')
							+ (a[3] || a[5] ? '|' : '')
					).split('|').filter(el => el.length);
				}

				return this._elements;
			}

			fillProp(prop, element) {
				var matches;
				var elProps = [];
				while (matches = this.regex[prop].exec(element)) {
					elProps.push(this.matches2val(prop, matches));
				}

				this['_' + prop].push(elProps.filter(
					v => typeof v === 'string'
						? v.length
						: v.n.length
				));
			}

			matches2val(prop, matches) {
				return this.isComplex(prop)
					? {n: matches[1], v: matches[3]}
					: (matches[3] || '') + (matches[5] || '');
			}

			isComplex(prop) {
				return this.complexProps.indexOf(prop) > -1;
			}

			static initProps() {
				var props = Object.keys(this.regex());

				props.forEach(function(prop) {
					prop !== 'elements'
						&& Object.defineProperty(
							this.prototype,
							prop,
							{get: this.getter(prop)}
						);

					Object.defineProperty(
						this.prototype,
						prop[0],
						{get: this.g(prop)}
					);
				}, this);
			}

//			this.classes = ['class0'];
			static getter(prop) {
				return function() {
					if (!this['_' + prop]) {
						this['_' + prop] = [];
						this.elements.forEach(
							el => this.fillProp(prop, el)
						);
					}

					return this['_' + prop];
				};
			}
//			this.c = 'class0';
			static g(prop) {
				return function() {
					return this.proxy[prop];
				}
			}
		}
		SelectorGetter.initProps();

		var instances = {};

		return function(selector) {
			return instances[selector]
				|| (instances[selector] = new SelectorGetter(selector));
		};
	})();

	if (typeof window !== 'undefined')
		window.SelectorGetter = SelectorGetter;

	return SelectorGetter;
});
