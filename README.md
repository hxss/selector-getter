# SelectorGetter.js

Simple class for parsing css selector and provide getters for its properties.
```
const element = new SelectorGetter('div#article54.article[data-name="data_val"]:not(n+1):before');

element.s = 'div#article54.article[data-name="data_val"]:not(n+1):before';
element.e = 'div#article54.article[data-name="data_val"]:not(n+1):before';
element.t = 'div';
element.i = 'article54';
element.c = 'article';
element.a.n = 'data-name';
element.a.v = 'data_val';
element.d.n = 'name';
element.d.v = 'data_val';
element.p[0].n = 'not';
element.p[0].v = 'n+1';
element.p[1].n = 'before';
```

## Usage

Constructor receive only selector string:

```
const sg = new SelectorGetter('div#header > nav.menu.second--class[data-name1="data_val1"] a[href]:not(button)');
```

SelectorGetter provides only getters for selector and realize singlton pattern.
So if you try to create new object from one selector several times - you get same object.

Selector object has this props(getters):

```
sg.selector
	= sg.s
	= String(sg)
	= '' + sg
	= 'div#header > nav.menu[data-name1='data_val1'] a[href]:not(button)';

sg.elements = sg.e = [
	"div#header",
	"nav.menu",
	"a[href]",
];
```

Each selector property is array of values for each element:

```
sg.tags = [
	['div'],
	['nav'],
	['a'],
];
sg.t = [
	'div',
	'nav',
	'a',
];

```
Short getters(e, t, i, c, a, d, p) is a greedy [RecursiveProxy](https://gitlab.com/hxss/RecursiveProxy) for SelectorGetter object that return each single array subitem as value of this array.
```
sg.ids = [
	['header'],
	[],
	[],
];
sg.i = [
	'header',
	[],
	[],
];
sg.classes = [
	[],
	['menu', 'second--class'],
	[],
];
sg.c = [
	[],
	['menu', 'second--class'],
	[],
];
sg.attrs = [
	[],
	[{n: "data-name1", v: "data_val1"}],
	[{n: "href", v: undefined}],
];
sg.a = [
	[],
	{n: "data-name1", v: "data_val1"},
	{n: "href", v: undefined},
];
sg.datas = [
	[],
	[{n: "name1", v: "data_val1"}],
	[],
];
sg.d = [
	[],
	{n: "name1", v: "data_val1"},
	[],
];
sg.pseudos = [
	[],
	[],
	[{n: "not", v: "button"}],
];
sg.p = [
	[],
	[],
	{n: "not", v: "button"},
];
```
Short getters is usefull when you work with const selector and need to refer to its property:
```
const element = new SelectorGetter('div#article54.article[data-name="data_val"]:not(n+1):before');

element.s = 'div#article54.article[data-name="data_val"]:not(n+1):before';
element.e = 'div#article54.article[data-name="data_val"]:not(n+1):before';
element.t = 'div';
element.i = 'article54';
element.c = 'article';
element.a.n = 'data-name';
element.a.v = 'data_val';
element.d.n = 'name';
element.d.v = 'data_val';
element.p[0].n = 'not';
element.p[0].v = 'n+1';
element.p[1].n = 'before';
```
